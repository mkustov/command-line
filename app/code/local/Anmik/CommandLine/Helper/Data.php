<?php

/**
 * Command line data helper
 *
 * @category    Anmik
 * @package     Anmik_CommandLine
 * @author      Mikhail Kustov <semulia@gmail.com>
 */
class Anmik_CommandLine_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Terminal error codes
     */
    const MISCELLANEOUS_ERROR_CODE = 1;
    const PERMISSION_PROBLEM_ERROR_CODE = 126;
    const COMMAND_NOT_FOUND_ERROR_CODE = 127;

    /**
     * Return error message by error code
     *
     * @param $errorCode
     * @return string
     */
    public function getErrorMessage($errorCode)
    {
        if ($errorCode) {
            switch ($errorCode) {
                case self::MISCELLANEOUS_ERROR_CODE:
                    $errorMessage = $this->__('The Matrix has you...');
                    break;
                case self::PERMISSION_PROBLEM_ERROR_CODE:
                    $errorMessage = $this->__('Permission problems.');
                    break;
                case self::COMMAND_NOT_FOUND_ERROR_CODE:
                    $errorMessage = $this->__('Command not found.');
                    break;
                default:
                    $errorMessage = $this->__('Wake up, Neo.');
                    break;
            }
            return $errorMessage;
        } else {
            return $this->__('Unknown error.');
        }
    }
}