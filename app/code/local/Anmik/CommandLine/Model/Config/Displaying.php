<?php

/**
 * Command line appearance config model
 *
 * @category   Anmik
 * @package    Anmik_CommandLine
 * @author     Mikhail Kustov <semulia@gmail.com>
 */
class Anmik_CommandLine_Model_Config_Displaying extends Mage_Core_Model_Config_Data
{
    /**
     * Possible command line displaying options
     */
    const DISPLAY_ALWAYS = 0;
    const DISPLAY_ON_PRESS_SHORTCUTS = 1;

    /**
     * Get possible appearance configuration options
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            self::DISPLAY_ALWAYS  => Mage::helper('commandline')->__('Display always'),
            self::DISPLAY_ON_PRESS_SHORTCUTS => Mage::helper('commandline')->__('Display after press shortcuts'),
        );
    }
}