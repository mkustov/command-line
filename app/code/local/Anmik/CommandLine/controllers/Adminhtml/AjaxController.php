<?php

/**
 * Command line ajax controller
 *
 * @category    Anmik
 * @package     Anmik_CommandLine
 * @author      Mikhail Kustov <semulia@gmail.com>
 */
class Anmik_CommandLine_Adminhtml_AjaxController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Ajax action for popup terminal
     *
     */
    public function terminalAction()
    {
         $this->loadLayout()->renderLayout();
    }

    /**
     * Ajax action for entered command
     *
     */
    public function commandAction()
    {
        $command = $this->getRequest()->getParam('command');
        $output = array();
        exec($command, $output, $errorCode);
        $response = $errorCode ? array(Mage::helper('commandline')->getErrorMessage($errorCode)) : $output;
        die(json_encode($response));
    }
}